from PIL import Image
import numpy
import os
import time
import get_model
import tools

__author__ = 'root'


def recognize(pic_path, trained_model, cnn_input_size, n_channels):
    print 'started recognizing of ' + pic_path
    start = time.time()
    stride = 20
    pixels = tools.get_pixels(0, 0, pic_path, do_ravel=False)
    result_pixels = numpy.zeros_like(pixels, dtype=int)
    width = pixels.shape[0]
    height = pixels.shape[1]
    steps_num = width/stride
    for i in range(0, width, stride):
        for j in range(0, height, stride):
            pixels_square = pixels[i:i+cnn_input_size, j:j+cnn_input_size]
            if pixels_square.shape[0] != cnn_input_size or pixels_square.shape[1] != cnn_input_size:
                break
            square = pixels_square.reshape(1, cnn_input_size, cnn_input_size, n_channels)
            y = trained_model(square)
            recognized_type = y.argmax()
            if recognized_type == 1:
                # road
                result_pixels[i:i+cnn_input_size, j:j+cnn_input_size] = pixels_square * 256
        print 'step ' + str(i/stride) + ' of ' + str(steps_num)
    result_image = Image.fromarray(result_pixels.astype('uint8'))
    save_path = os.path.splitext(pic_path)[0] + 'rec' + '.jpg'
    result_image.save(save_path)
    end = time.time()
    print 'end recognizing, saved to ' + save_path
    print 'time elapsed: ' + str(end - start)

model = get_model.get_cnn_model('road1-26.pkl')
# recognize('res/0.bmp', model, 30, 3)
recognize('res/1123.bmp', model, 30, 3)
