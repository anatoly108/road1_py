import os
import random
import csv
import imp


def get_dataset_rows():

    size = 30
    crop_save_path = 'res/crop/'
    source_csv_path = 'res/0.csv'
    # should be like this to provide easy dataset copying
    tools = imp.load_source('tools', '/home/anatoly/do/road1_py/tools.py')

    if not os.path.exists(crop_save_path):
        os.makedirs(crop_save_path)

    source_csv = open(source_csv_path, 'rt')

    reader = csv.reader(source_csv, delimiter=',', quotechar='|')
    reader.next()  # because we have headers
    examples = []
    for row in reader:
        x = int(row[0])
        y = int(row[1])
        out = int(row[2])
        path = row[3]

        examples.append([x, y, out, path])

    random.shuffle(examples)

    rows = []
    answers = []
    for example in examples:
        x = example[0]
        y = example[1]
        out = example[2]
        image_path = example[3]
        pixels = tools.get_pixels(x, y, image_path, size, True, crop_save_path, grey=False)
        rows.append(pixels)
        out_vector = [0] * len(example)
        out_vector[out] = 1
        answers.append(out_vector)

    source_csv.close()
    return [rows, answers]
