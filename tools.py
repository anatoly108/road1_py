import Image
from PIL import ImageFilter
import numpy
import os

delimiter = ' '

__author__ = 'anatoly'


def get_pixels(x, y, path, size=0, is_need_save=False, save_path='', grey=False, do_ravel=True):
    img = Image.open(path)
    if size == 0:
        (width, height) = img.size
    else:
        width = size
        height = width
    left = x
    top = y
    box = (left, top, left + width, top + height)

    if grey:
        img = img.convert('L')
    img = img.filter(ImageFilter.SHARPEN)
    area = img.crop(box)
    I = numpy.asarray(area) / 256.

    if do_ravel:
        I = I.ravel()

    if is_need_save:
        join = os.path.join(save_path, 'crop' + str(x) + '_' + str(y) + '.jpeg')
        area.save(join, 'jpeg')

    return I


def get_pixels_string(pixels_array):

    row = str(''.join(str(row) + delimiter for row in pixels_array)).strip()

    return row