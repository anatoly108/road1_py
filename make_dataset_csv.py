import time
import get_dataset_rows
import tools

__author__ = 'root'

print 'start making dataset'
start = time.time()

result_csv_path = 'data0.csv'

result_csv = open(result_csv_path, "w")
[rows, answers] = get_dataset_rows.get_dataset_rows()
for i in range(0, len(rows)):
    row = rows[i]
    answer = answers[i]
    # answer[0] - because now the answer is the vector, for example [1 0]
    string = str(answer[0]) + tools.delimiter + tools.get_pixels_string(row)
    result_csv.write(string + "\n")
result_csv.close()

end = time.time()
print 'end making dataset'
print 'time elapsed: ' + str(end - start)