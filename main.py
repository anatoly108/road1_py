__author__ = 'anatoly'
from theano import *
from theano import pp
import theano.tensor as T

x = T.dscalar('x')
y = T.dscalar('y')
z = x + y
f = function([x, y], z)

print f(2, 3)
print pp(z)

x = T.dmatrix('x')
y = T.dmatrix('y')
z = x + y
f = function([x, y], z)
print f([[1, 2], [3, 4]], [[3, 1], [2, 5]])

# exercise
# Modify and execute this code to compute this expression: a ** 2 + b ** 2 + 2 * a * b.
a = theano.tensor.vector()  # declare variable
b = theano.tensor.vector()
out = a ** 2 + b ** 2 + 2 * a * b              # build symbolic expression
f = theano.function([a, b], out)   # compile function
print f([0, 1, 2], [5, 2, 1])  # prints `array([0, 2, 1026])`

x = T.dmatrix('x')
s = 1 / (1 + T.exp(-x))
logistic = function([x], s)
print logistic([[0, 1], [-1, -2]])

a, b = T.dmatrices('a', 'b')
diff = a - b
abs_diff = abs(diff)
diff_squared = diff**2
f = function([a, b], [diff, abs_diff, diff_squared])
print f([[1, 1], [1, 1]], [[0, 1], [2, 3]])