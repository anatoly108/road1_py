import get_model

import tools


__author__ = 'anatoly'

f = get_model.get_cnn_model('road1.pkl')
# "input space" is actually: 'b',0,1,'c'

# sky
# img_ = tools.get_pixels(0, 0, "res/0.bmp", 30, False, '').reshape(1, 30, 30, 3)

# road
# img_ = tools.get_pixels(238, 447, "res/0.bmp", 30, False, '').reshape(1, 30, 30, 3)

# grass, similar to road
# img_ = tools.get_pixels(85, 239, "res/0.bmp", 30, False, '').reshape(1, 30, 30, 3)

# green grass
img_ = tools.get_pixels(577, 289, "res/0.bmp", 30, False, '').reshape(1, 30, 30, 3)

y = f(img_)
print y

# todo: write tests
# todo: learn how to plot the data