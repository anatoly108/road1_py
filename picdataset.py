import numpy
import imp

from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix


__author__ = 'root'

delimiter = ' '


class PicDataset(DenseDesignMatrix):
    def __init__(self,
                 path='/home/anatoly/do/road1_py/data0.csv',
                 axes=['b', 0, 1, 'c'],
                 one_hot=False,
                 with_labels=True,
                 start=None,
                 stop=None,
                 preprocessor=None,
                 fit_preprocessor=False,
                 fit_test_preprocessor=False):
        self.n_classes = 3
        self.img_shape = (3, 30, 30)
        self.img_size = numpy.prod(self.img_shape)
        self.args = locals()

        get_dataset_rows = imp.load_source('get_dataset_rows', '/home/anatoly/do/road1_py/get_dataset_rows.py')
        [X, y] = get_dataset_rows.get_dataset_rows()
        X = numpy.asarray(X)
        y = numpy.asarray(y)

        super(PicDataset, self).__init__(X=X, y=y)